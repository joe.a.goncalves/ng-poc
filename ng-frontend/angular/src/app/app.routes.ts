import {Routes} from '@angular/router';
import {ReviewComponent} from './pages/review/review.component';
import {SignUpComponent} from './pages/sign-up/sign-up.component';

export const appRoutes: Routes = [
  { path: 'review/:id', component: ReviewComponent },
  { path: 'sign-up',      component: SignUpComponent },
  { path: '',
    redirectTo: '/sign-up',
    pathMatch: 'full'
  },
  { path: '**', redirectTo: '/sign-up' }
];
