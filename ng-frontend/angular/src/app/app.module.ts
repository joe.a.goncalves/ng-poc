import { BrowserModule } from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import { ReviewComponent } from './pages/review/review.component';
import {TrackingService} from './services/tracking.service';
import {RouterModule} from '@angular/router';
import {appRoutes} from './app.routes';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {DataService} from './services/data.service';
export function init_app(trackingService) {
  return () => trackingService.getTrackingGuid();
}
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SignUpComponent,
    ReviewComponent,
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    TrackingService,
    SignUpComponent,
    CookieService,
    DataService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [TrackingService], multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
