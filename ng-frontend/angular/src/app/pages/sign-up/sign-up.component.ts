import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, NgForm, Validators} from '@angular/forms';
import {CustomerService} from '../../services/customer.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  formGroup: FormGroup;
  submitted = false;

  constructor(private fb: FormBuilder, private signUpService: CustomerService, private router: Router) {
  }

  ngOnInit() {
    this.buildForm();
  }

  get f() {
    return this.formGroup.controls;
  }

  buildForm() {
    this.formGroup = this.fb.group({
      firstname: ['', [Validators.minLength(2), Validators.maxLength(50), Validators.required, Validators.pattern(/^[A-z  -']+$/)]],
      lastname: ['', [Validators.minLength(2), Validators.maxLength(50), Validators.required, Validators.pattern(/^[A-z  -']+$/)]],
      email: ['', [Validators.minLength(2), Validators.maxLength(50), Validators.required, Validators.pattern(/^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/)]],
      zip: ['', [Validators.minLength(5), Validators.maxLength(5), Validators.required, Validators.pattern(/^\d{5}$/)]],
    });
  }

  submit(form: NgForm) {
    if (form.invalid) {
      return this.submitted = true;
    }
    const userModel = form.value;
    this.signUpService.post(userModel).subscribe(userId => {
      this.router.navigate(['review', userId]);
    });
  }
}
