import { Component, OnInit } from '@angular/core';
import {CustomerService} from '../../services/customer.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-review',
  templateUrl: './review.component.html',
  styleUrls: ['./review.component.scss']
})
export class ReviewComponent implements OnInit {
  data: any = {};
  constructor(private customers: CustomerService, private route: ActivatedRoute) {

  }


  ngOnInit() {
    this.customers.get(this.route.snapshot.params.id).subscribe(data => this.data = data);
  }

}
