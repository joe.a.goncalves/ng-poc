import {Injectable} from '@angular/core';

import {CookieService} from 'ngx-cookie-service';
import {DataService} from './data.service';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TrackingService {

  constructor(private http: HttpClient, private cookies: CookieService, private dataService: DataService) {
  }

  getTrackingGuid() {
    return new Promise((resolve, reject) => {
      if (this.cookies.check('tracking_guid')) {
        this.dataService.data.trackingGuid = this.cookies.get('tracking_guid');
        return resolve();
      }
      const url = `${environment.api}/generate_guid`;
      this.http.get(url, {responseType: 'text'}).subscribe(data =>{
        this.dataService.data.trackingGuid = data;
        this.cookies.set('tracking_guid', data);
        resolve();
      });
    });
  }
}
