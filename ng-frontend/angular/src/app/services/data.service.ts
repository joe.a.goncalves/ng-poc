import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  // In real life there'd be a redux store
  data: any = {} ;
  constructor() { }
}
