import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DataService} from './data.service';
import {environment} from '../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private http: HttpClient, private dataService: DataService) {
  }

  post(userModel: any) {
    userModel.tracking_guid = this.dataService.data.trackingGuid;
    const url = `${environment.api}/customers`;
    return this.http.post(url, userModel, {responseType: 'text'});
  }
  get(userId: number){
    const url = `${environment.api}/customers/${userId}`;
    return this.http.get(url);
  }
}
