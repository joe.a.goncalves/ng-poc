const Schema = require('validate');
module.exports = new Schema({
    firstname: {
        type: String,
        required: true
    },
    lastname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    zip:{
        type: String,
        required: true
    }
}) ;
