var express = require('express');
var router = express.Router();

const {sql, request} = require('../lib/database');
const customerValidationSchema = require('../validation-schemas/customer');
/* GET customer listing. */
router.get('/', async function (req, res, next) {
    try {
        const dbreq = await request();
        const result = await dbreq.query('select * from credmo.dbo.customer');
        res.send(result.recordset);
    } catch (ex) {
        res.send(ex.message).status(500);
    }
});
router.get('/:id', async function (req, res, next) {
    try {
        const dbreq = await request();
        await dbreq.input('id', sql.BigInt, req.params.id).query('select * from credmo.dbo.customer where id = @id')
            .then(result => {
                res.send(result.recordset[0]);
            })
            .catch(console.error)
        ;
    } catch (ex) {
        res.send(ex.message).status(500);
    }
});
router.post('/', async (req, res, next) => {
    try {
        let errors = customerValidationSchema.validate(req.body);
        if (errors.length) {
            res.json(errors);
            return res.status(400)
        }
        const dbreq = await request();
        let result = await dbreq
            .input('insertdt', sql.DateTime, new Date())
            .input('first_name', sql.VarChar(20), req.body.firstname)
            .input('last_name', sql.VarChar(20), req.body.lastname)
            .input('email', sql.VarChar(20), req.body.email)
            .input('password', sql.VarChar(25), req.body.password)
            .input('tracking_guid', sql.UniqueIdentifier, req.body.tracking_guid)
            .query(`
            insert into customer (first_name, last_name, email, password, tracking_guid)
                values(@first_name, @last_name, @email, @password, @tracking_guid);
                SELECT SCOPE_IDENTITY() as id;
        `);
        const id = result.recordset[0].id;
        res.send(id.toString());
    } catch (ex) {
        res.send(ex.message).status(500);
    }
}
})
;

module.exports = router;
